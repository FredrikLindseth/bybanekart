# Stilisert bybanekart

Geografisk korrekt bybanekart, stilisert med fargene i Tubemap.

Linjene og stoppene er hentet fra OpenStreetMap, via Overpass, puttet inn i QGIS 3.4 hvor alt ble rotert 45 grader for å passe bedre på ett ark og så stilet for å ligne.

Videre er sporene og linjene lagret som SVG og tatt inn i Inkscape 0.92 hvor stoppene ble navngitt.

Det endelige resultatet er `bybanekart.svg` som vist under.


![bybanekart](bybanekart.png)

## Oppdatere kartet


### Hent data

For å hente ut Bybanestopp, gjør denne spørringen med Overpass

[link her](https://overpass-turbo.eu/s/Efg)

```overpassql
[out:json][timeout:250];
{{geocodeArea:Bergen}}->.searchArea;
(
  node["light_rail"="yes"](area.searchArea);
);
out body;
>;
out skel qt;
```

For å hente ut linjene, gjør denne spørringen med Overpass

[link her](https://overpass-turbo.eu/s/Efh)

```overpassql
[out:json][timeout:250];
{{geocodeArea:Bergen}}->.searchArea;
(
  way["railway"="light_rail"](area.searchArea);
  way["name"="Bybanen"](area.searchArea);
);
out body;
>;
out skel qt;
```

Trykk eksporter og velg geojson og dra datafilene inn i QGIS.

Man må gjerne fjerne noen biter som depotene og eventuelle bybanelinjer man ikke vil ha med / som ikke er bygget enda.

### Tegnestil

I teorien har man to lag i QGIS. 

I `Layers`-listen til høyre i QGIS, høyreklikk på laget som inneholder bybanesporet, `Properties`, gå til `Symbology` og lag linjen en fin blåfarge feks `#10069f` og sett tykkelsen til linjen, feks `0.66`mm

Gå inn på symbology på laget med stoppene og velg `Simple marker` med `Stroke colour` feks `#dd1f19` og en `Stroke width` på feks `0.8`mm og hvit som `fill colour`

For å slå på navn på stoppene går man ikke til `Symbology`, men til `Labels` og velger `Single labels` og setter `Label with` til å være `name` fra OpenStreetMap-dataene. Nå dukker navnene opp, men dårlig plassert.

Trykk på `Placement` og velg `Cartographic` og sett ``Distance offset from` til å være `From symbol bounds`. Da vil navnet blii plassert utenfor sirkel-symbolet på stoppet og ikke fra senter.

Nå skal det se likt ut. 

For å rotere kartet (for å passe bedre på ett ark eller i en ramme) går man ut i QGIS sitt hovedvindu, og så på bunnen av vinduet er det en boks som heter `Rotation`.  Ved å sette den til `-45` grader vil det passe fint.

[Bybanekart](bybanekart.png)-bildet er `stopp.svg` og `spor.svg` tatt inn i Inkscape og så er teksten tegnet på manuelt.

Voila!

Fonten som er brukt er [Railway sans](https://fontlibrary.org/en/font/railway-sans)